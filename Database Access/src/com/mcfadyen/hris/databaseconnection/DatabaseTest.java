package com.mcfadyen.hris.databaseconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DatabaseTest {

	// public void SelectEmployees(ArrayList<String> arraylist){
	public static void main(String[] args) {
		Connection connection = null;
		Statement st = null;
		ResultSet resultSet;
		try {
            DatabaseAccess databaseAccess = new DatabaseAccess();
			connection = databaseAccess.getConnectionPool("mysql","hris");
			st = connection.createStatement();
			resultSet = st.executeQuery("SELECT *  FROM personal_details");
			while (resultSet.next()) {
				System.out.println(resultSet.getString("emp_id"));
				System.out.println(resultSet.getString("first_name"));
			}
			databaseAccess.closeAll(connection, st,resultSet);
		} catch (Exception err) {

			err.printStackTrace();
		}
	}

	/*
	 * return null;
	 * 
	 * }
	 */
}

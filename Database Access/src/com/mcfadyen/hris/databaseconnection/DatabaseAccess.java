package com.mcfadyen.hris.databaseconnection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

import com.mcfadyen.hris.util.PropertyUtilNew;




public class DatabaseAccess {
	private String userName;
	private String URL;
	private String password;
	private String driverName;
	private static DataSource ds;

	private DBDetailsBean DatabaseDetails(String databaseName) throws IOException {

		Properties properties = new Properties();
		properties = PropertyUtilNew.getPropertiesFromClasspath("/databaseconnection.properties");
		this.userName = properties.getProperty(databaseName + ".username");
		this.URL = properties.getProperty(databaseName + ".URL");
		this.password = properties.getProperty(databaseName + ".password");
		this.driverName = properties.getProperty(databaseName + ".drivername");
		System.out.println(userName);
		System.out.println(URL);
		System.out.println(password);
		System.out.println(driverName);
		DBDetailsBean dbDetailsBean = new DBDetailsBean();
		dbDetailsBean.setUserName(userName);
		dbDetailsBean.setURL(URL);
		dbDetailsBean.setPassword(password);
		dbDetailsBean.setDriverName(driverName);
		return dbDetailsBean;
	}

	
	public Connection databaseConnection(String databaseName,String schemaName) throws IOException {
		DBDetailsBean dbDetailsBean = new DatabaseAccess().DatabaseDetails(databaseName);
		Connection connenction = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(dbDetailsBean.getDriverName());
			connenction = DriverManager.getConnection(dbDetailsBean.getURL()+schemaName, dbDetailsBean.getUserName(),dbDetailsBean.getPassword());
		} catch (SQLException se) {

			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connenction;

	}
	public Connection getConnectionPool(String databaseName,String schemaName) throws IOException{
		Connection connenction = null;
		DBDetailsBean dbDetailsBean = new DatabaseAccess().DatabaseDetails(databaseName);
		BasicDataSource dataSource=null;
		
		switch(databaseName){
		case "oracle":
			dataSource=OracleDataSource.getInstance(dbDetailsBean,schemaName);
		case "mysql":
			dataSource=MysqlDataSource.getInstance(dbDetailsBean,schemaName);
		case "sql":
			dataSource=SqlDataSource.getInstance(dbDetailsBean,schemaName);
		case "PostgreSQL":
			dataSource=PostgreSQLDataSource.getInstance(dbDetailsBean,schemaName);
		case "JavaDB":
			dataSource=JavaDBDataSource.getInstance(dbDetailsBean,schemaName);
		case "SQLite":
			dataSource=SQLiteDataSource.getInstance(dbDetailsBean,schemaName);
		
		}
		try {
			connenction=dataSource.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connenction;
		
	}

	public Connection getDataSource(String databaseName,String shemaName) throws ServletException, SQLException{
		try {
			InitialContext initContext = new InitialContext();
			Context env = (Context) initContext.lookup("java:comp/env");
			String database=(databaseName+"/"+shemaName);
			ds = (DataSource) env.lookup(database);
			
		} catch (NamingException e) {
			throw new ServletException();
		}
		return ds.getConnection();
	}
	public void closeAll(Connection connection) {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}

	public void closeAll(Connection connection, Statement statement) {
		try {
			if (statement != null)
				statement.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}

	public void closeAll(Connection connection, PreparedStatement preparedStatement) {
		try {
			if (preparedStatement != null)
				preparedStatement.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}

	public void closeAll(Connection connection, Statement statement, PreparedStatement preparedStatement) {
		try {
			if (statement != null)
				statement.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (preparedStatement != null)
				preparedStatement.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}
	public void closeAll(Connection connection, Statement statement,ResultSet resultSet) {
		try {
			if (resultSet != null)
				resultSet.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (statement != null)
				statement.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}
	
	public void closeAll(Connection connection, PreparedStatement preparedStatement,ResultSet resultSet) {
		try {
			if (resultSet != null)
				resultSet.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (preparedStatement != null)
				preparedStatement.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}
	public void closeAll(Connection connection, Statement statement,PreparedStatement preparedStatement,ResultSet resultSet) {
		try {
			if (resultSet != null)
				resultSet.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (statement != null)
				statement.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (preparedStatement != null)
				preparedStatement.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}
}

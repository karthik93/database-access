package com.mcfadyen.hris.databaseconnection;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

public class MysqlDataSource extends BasicDataSource{
private static BasicDataSource dataSourceInstance=new MysqlDataSource();
	
	private MysqlDataSource(){
		
	}
	 public static BasicDataSource getInstance(DBDetailsBean dbDetailsBean,String schemaName) {
	      if(dataSourceInstance == null) {
	    	  dataSourceInstance = new MysqlDataSource();
	      }
	      dataSourceInstance.setDriverClassName(dbDetailsBean.getDriverName());
	      dataSourceInstance.setUrl(dbDetailsBean.getURL()+schemaName);
	      dataSourceInstance.setUsername(dbDetailsBean.getUserName());
	      dataSourceInstance.setPassword(dbDetailsBean.getPassword());
	      
	      return dataSourceInstance;
	   }
	
}

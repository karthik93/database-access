package com.mcfadyen.hris.databaseconnection;

import java.io.IOException;
import javax.servlet.Filter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class DatabaseDemo
 */
@WebServlet("/DatabaseDemo")
public class DatabaseDemo extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static DataSource ds;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DatabaseDemo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		Connection conn = null;
		Statement statement = null;
		ResultSet resultSet = null;
		PrintWriter out = response.getWriter();
		try {
			conn = new DatabaseAccess().getDataSource("mysql","hris");
			statement = conn.createStatement();
			String sql = "SELECT * FROM personal_details";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				String firstName = resultSet.getString("first_name");
				String emp_id = resultSet.getString("emp_id");

				out.println(emp_id);
				out.println(firstName);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		out.println("connected to database");

		try {
			resultSet.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			statement.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			conn.close();
		} catch (SQLException e11) {
			// TODO Auto-generated catch block
			e11.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	public void init(ServletConfig config) throws ServletException {
		
		
	}
	public Connection getDataSource(String databaseName,String shemaName) throws ServletException, SQLException{
		try {
			InitialContext initContext = new InitialContext();
			Context env = (Context) initContext.lookup("java:comp/env");
			String database=(databaseName+"/"+shemaName);
			ds = (DataSource) env.lookup(database);
			
		} catch (NamingException e) {
			throw new ServletException();
		}
		return ds.getConnection();
	}

}

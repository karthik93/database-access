package com.mcfadyen.hris.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;

public class ReflectionUtil {
	
	public static Object CreateBean(Class beanClassObject, HttpServletRequest request ){
		int index=0,length;
		String[] name;
		
		Object beanObject = null;
//		Class[] paramString = new Class[1];	
//		paramString[0] = String.class;
		
		try {
			
             beanObject=beanClassObject.getConstructor().newInstance();
			Method[] methods=beanClassObject.getMethods();
			length=methods.length;
			
			
			
			 name=new String[length];
		
		
			for(Method method : methods){
				
                 if(isSetter(method)) {
			    name[index]=method.getName();	
			  //  System.out.println(name[index]);
                	 
		    	String variable=ConvertToVariable(name[index]);
		    //	System.out.println(variable);
		    	//method = beanClassObject.getDeclaredMethod(name[index], paramString);
		    	System.out.println(name[index]);
				index++;
				method.invoke(beanObject, request.getParameter(variable));
				
			}
			
			}
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Exception in Reflection class");
		}
		
		return beanObject;
		
	}
	
	
	
	public static boolean isSetter(Method method) {
		if (!method.getName().startsWith("set"))
			return false;
		if (method.getParameterTypes().length != 1)
			return false;
		return true;
	}
	
	public static String ConvertToVariable(String methodName){
		return (methodName.charAt(3)+"").toLowerCase()+methodName.substring(4);
		
	}

}

package com.mcfadyen.hris.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.io.*;



public class FileUtil {
	 public static String readFileAsString(String filename) 
			  throws IOException
			  {
			    BufferedReader reader = new BufferedReader(new FileReader(filename));
			    String line;
			    StringBuilder sb = new StringBuilder();
			    while ((line = reader.readLine()) != null)
			    {
			      sb.append(line + "\n");
			    }
			    reader.close();
			    return sb.toString();
			  }
			   
			  public static List<String> readFileAsListOfStrings(String filename) throws Exception
			  {
			    List<String> records = new ArrayList<String>();
			    BufferedReader reader = new BufferedReader(new FileReader(filename));
			    String line;
			    while ((line = reader.readLine()) != null)
			    {
			      records.add(line);
			    }
			    reader.close();
			    return records;
			  }
			  
			  public static void copyFile(File source, File destination) throws IOException
			  {
			    byte[] buffer = new byte[100000];
			 
			    BufferedInputStream bufferedInputStream = null;
			    BufferedOutputStream bufferedOutputStream = null;
			    try
			    {
			      bufferedInputStream = new BufferedInputStream(new FileInputStream(source));
			      bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(destination));
			      int size;
			      while ((size = bufferedInputStream.read(buffer)) > -1)
			      {
			        bufferedOutputStream.write(buffer, 0, size);
			      }
			    }
			    catch (IOException e)
			    {
			      throw e;
			    }
			    finally
			    {
			      try
			      {
			        if (bufferedInputStream != null)
			        {
			          bufferedInputStream.close();
			        }
			        if (bufferedOutputStream != null)
			        {
			          bufferedOutputStream.flush();
			          bufferedOutputStream.close();
			        }
			      }
			      catch (IOException ioe)
			      {
			        throw ioe;
			      }
			    }
			  }
			  
			  public static void mergeFiles(File filefirst, File filesecond, File mergedFile) {
				  
				  File[] files = new File[2];
					files[0] = filefirst;
					files[1] = filesecond;
					//File mergedFile = new File(mergedFilePath);
					FileWriter fstream = null;
					BufferedWriter out = null;
					try {
						fstream = new FileWriter(mergedFile, true);
						 out = new BufferedWriter(fstream);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
			 
					for (File f : files) {
						//System.out.println("merging: " + f.getName());
						FileInputStream fis;
						try {
							fis = new FileInputStream(f);
							BufferedReader in = new BufferedReader(new InputStreamReader(fis));
			 
							String aLine;
							while ((aLine = in.readLine()) != null) {
								out.write(aLine);
								out.newLine();
							}
			 
							in.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
			 
					try {
						out.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			 
				}
			  public static void main(String[] args) throws Exception {
				  File infile =new File("G:\\myfile.txt");
		    	    File outfile =new File("G:\\myfile2.txt");
		    	    File merge= new File("G:\\mydest.txt");
		    	    FileUtil.mergeFiles(infile, outfile, merge);
				try {
					FileUtil.copyFile(infile, outfile);
					System.out.println("copiedd!!");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(FileUtil.readFileAsString("G:\\myfile.txt"));
				System.out.println(FileUtil.readFileAsListOfStrings("G:\\myfile.txt"));
			}
}

package com.mcfadyen.hris.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


 
public class PropertyUtils {
  private final Logger logger = LoggerFactory.getLogger(PropertyUtils.class);
 
 public Properties getLdapProperties() throws IOException{
  try{
   Properties prop = new Properties();
   InputStream input = getClass().getResourceAsStream("/ldaputil.properties");
   prop.load(input);
   input.close();
   return prop;
  }catch(NullPointerException e){
   
   logger.info("NullPointerException - ldaputil.properties nopt found");
  }
   return null;
 }
}
package com.mcfadyen.hris.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLType;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.Format;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

public class DateUtil {
 public static Date convertStringToDate(String dateString) {
        Date date = null;
        try
        {
                    //create SimpleDateFormat object with source string date format
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                    sdf.setLenient(false);
                    //parse the string into Date object
                    date = sdf.parse(dateString);
                  //  System.out.println("Date :"+date);
        }catch(ParseException pe){
                    System.out.println("Parse Exception : " + pe);
        } catch (java.text.ParseException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  }
  return date;
}
 
 public  static Date convertDateFormat(String fromDate) throws java.text.ParseException{
        String convertedDate = "";
        Date date = null;
        try
        {
                    //create SimpleDateFormat object with source string date format
                    SimpleDateFormat sdfFrom = new SimpleDateFormat("dd/MM/yy");
                    
                    sdfFrom.setLenient(false);
                    //parse the string into Date object
                    date = sdfFrom.parse(fromDate);
                    //create SimpleDateFormat object with desired date format
                    SimpleDateFormat sdfTo = new SimpleDateFormat("dd-MMM-yyyy");
                    //parse the date into another format
                    convertedDate = sdfTo.format(date);
                    date = sdfTo.parse(convertedDate);
                  
        }catch(ParseException pe){
            System.out.println("Parse Exception : " + pe);
} catch (java.text.ParseException e) {
 // TODO Auto-generated catch block
 e.printStackTrace();
}
        return date;
}

	
	public enum test{
		ddMMMyyyy("asd"),
		b("cc");
		
		private String v;
		test(String a){
			this.v = a;
		}
	}
	
	
	public static String dateToFormat(Date date)
	{
		Format formatter = new SimpleDateFormat("dd-MMM-yy");test p=test.b;
	    String s = formatter.format(date);
	   // System.out.println(s);
	    return s;
	}
	public static void main(String[] args) throws FileNotFoundException, IOException, java.text.ParseException {
		System.out.println(DateUtil.convertStringToDate("23-nov-2015"));
		System.out.println(DateUtil.convertDateFormat("23/12/2015"));
		Date date= DateUtil.convertDateFormat("23/12/2015");
		System.out.println(DateUtil.dateToFormat(date));
	}
}
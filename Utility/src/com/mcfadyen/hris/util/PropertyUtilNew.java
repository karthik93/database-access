package com.mcfadyen.hris.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.gson.Gson;
import com.mcfadyen.hris.metrics.Metrics;

public class PropertyUtilNew {

	public Properties getLdapProperties() throws IOException {
		try {
			Properties prop = new Properties();
			InputStream input = getClass().getResourceAsStream("/ldaputil.properties");
			prop.load(input);
			input.close();
			return prop;
		} catch (NullPointerException e) {
			e.printStackTrace();
			// logger.info("NullPointerException - ldaputil.properties nopt
			// found");
		}
		return null;
	}
	
	//json to map
	
	public static Map<String, String> jsonToMap(JSONObject json) throws JSONException {
	    Map<String, String> retMap = new HashMap<String, String>();

	    if(json != JSONObject.NULL) {
	        retMap = toMap(json);
	    }
	    return retMap;
	}

	public static Map<String, String> toMap(JSONObject object) throws JSONException {
	    Map<String, String> map = new HashMap<String, String>();

	    Iterator<String> keysItr = object.keys();
	    while(keysItr.hasNext()) {
	        String key = keysItr.next();
	        Object value = object.get(key);

	        if(value instanceof JSONArray) {
	            value = toList((JSONArray) value);
	        }

	        else if(value instanceof JSONObject) {
	            value = toMap((JSONObject) value);
	        }
	        map.put(key, (String) value);
	    }
	    return map;
	}
	
	public static List<Object> toList(JSONArray array) throws JSONException {
	    List<Object> list = new ArrayList<Object>();
	    for(int i = 0; i < array.length(); i++) {
	        Object value = array.get(i);
	        if(value instanceof JSONArray) {
	            value = toList((JSONArray) value);
	        }

	        else if(value instanceof JSONObject) {
	            value = toMap((JSONObject) value);
	        }
	        list.add(value);
	    }
	    return list;
	}
	//json to map ended

	public static JSONObject propertyToJSON(String propFileName) throws IOException {
		PropertyUtilNew pu = new PropertyUtilNew();
		Gson gson = new Gson();
		JSONObject json = null;
		try {
			json = new JSONObject(gson.toJson(pu.getPropertiesFromClasspath(propFileName)));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;

	}
/**
 * 
 * @param propFileName
 * @return
 * @throws IOException
 */
	public static Properties getPropertiesFromClasspath(String propFileName) throws IOException {
		Properties props = new Properties();
		InputStream inputStream = null;
		try {
			inputStream= PropertyUtilNew.class.getResourceAsStream(propFileName);
			//inputStream = getClass().getResourceAsStream(propFileName);
			if (inputStream == null) {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
			props.load(inputStream);
			// System.out.println(props);
		} finally {
			inputStream.close();
		}
		return props;
	}

	public  static Map<String, String> readPropertyFileToMap(String propFileName) {
		HashMap<String, String> propvals = new HashMap<String, String>();
		try {
			Properties prop = new Properties();
			new PropertyUtilNew();
			prop = PropertyUtilNew.getPropertiesFromClasspath(propFileName);
			
			Set<String> propertyNames = prop.stringPropertyNames();
			for (String Property : propertyNames) {
				//System.out.println(Property + ":" + prop.getProperty(Property));
				propvals.put(Property, prop.getProperty(Property));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return propvals;
	}
	 public static List getKeysFromValue(Map hm,Object value){
		    Set ref = hm.keySet();
		    Iterator it = ref.iterator();
		    List list = new ArrayList();

		    while (it.hasNext()) {
		      Object o = it.next(); 
		      if(hm.get(o).equals(value)) { 
		        list.add(o); 
		      } 
		    } 
		    return list;
	 }

	public static void main(String[] args) throws FileNotFoundException, IOException, JSONException {
		Properties prop = new Properties();
		prop = PropertyUtilNew.getPropertiesFromClasspath("/ldaputil.properties");
		Metrics metrics=new Metrics();
		JSONObject json=metrics.getMetrics();
		Map mapp=toMap(json);
		System.out.println(mapp.keySet());
		Map<String, String> map= PropertyUtilNew.readPropertyFileToMap("/ldaputil.properties");
		// System.out.println(prop);
		System.out.println(prop.getProperty("LDAP_SERVER"));
		System.out.println(map);
		System.out.println(PropertyUtilNew.propertyToJSON("/ldaputil.properties"));
		Map<String, String> maps =PropertyUtilNew.readPropertyFileToMap("/column_names.properties");
		//System.out.println("********************");
		System.out.println(PropertyUtilNew.getKeysFromValue(maps, "Employee ID"));
		
		
	}

}
